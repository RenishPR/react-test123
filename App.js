import logo from './logo.svg';
import './App.css';
import Axios from "axios"
import React, { useEffect, useState } from 'react';

async function getWeather() {
  let result = await Axios.get("http://api.weatherapi.com/v1/forecast.json?key=698dc13c77094cf187695058201212&q=London&days=1")
  if(result){
    return result.data
  }else{
    return {error: "Something went wrong..."}
  }
}

class App extends React.Component {
  constructor(){
    super()
    this.state = {
      data: []
    }
  }

  async componentDidMount(){
    let data = await getWeather()
    if(data){
      console.log("success", data)
      this.setState({data})
    }else{
      this.setState({data})
    }
  }
 
  render(){
    const {data} = this.state
    return (
      <div className="App">
        <header className="App-header">
          {
            Object.keys(data).length>0?
            <>
              <div className='outerborder'>
                <div className='innerHead'>
                  <p className='paragraph_head'>Temp {data.current.temp_c} °C</p>
                    <h5>{data.current.condition.text}</h5>
                    <img src={data.current.condition.icon} alt='Icon1'></img><br />
                    <p className='paragraph_head'>
                    Place: {data.location.name} <br />
                            {data.location.country}
                      </p>
                </div>
                <div className='innerBody'>
                  {
                    data.forecast.forecastday[0].hour.map((items) => (
                      <>
                      <div className='loop-cell'>
                        <p className='paragraph'>Temp {items.temp_c} °C</p>
                        <h5>{items.condition.text}</h5>
                        <img src={items.condition.icon} alt='Icon1'></img>
                        <p className='paragraph'> on {items.time.split(" ")[1]}</p>
                      </div>
                        
                      </>
                    ))
                  }
                </div>
                
              </div>
              
            </>
            :<>
              <p>Something went wrong while calling the api</p>
            </>
          }
         
          
        </header>
      </div>
    );
  }
  
}

export default App;
